package id.trafood.watembaker;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    EditText editText,etCustom;
    TextView btnCustom;
    Button button;
    String nomorhp;
    String tembak;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.etNoHP);
        button = (Button) findViewById(R.id.btnTembak);
        etCustom = (EditText) findViewById(R.id.etCustomText);
        btnCustom = (TextView) findViewById(R.id.btnSendWithMessage);
        btnCustom.setText("Tulis pesan manual");
        btnCustom.setTextColor(getResources().getColor(R.color.colorPrimary));

        realm.init(MainActivity.this);
        realm = Realm.getDefaultInstance();
        // RealmResults are "live" views, that are automatically kept up to date, even when changes happen
        // on a background thread. The RealmBaseAdapter will automatically keep track of changes and will
        // automatically refresh when a change is detected.
        RealmResults<Task> tasks = realm.where(Task.class).findAll();
        final TaskAdapter adapter = new TaskAdapter(this,tasks);

        ListView listView = (ListView) findViewById(R.id.task_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Task task = (Task) adapterView.getAdapter().getItem(i);
                final EditText taskEditText = new EditText(MainActivity.this);
                taskEditText.setText(task.getName());
                final AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Edit No Telepon")
                        .setView(taskEditText)
                        .setPositiveButton("chat", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //TODO : this fore save edited task
                                editText.setText(task.getName());
                                dialogInterface.dismiss();
                            }
                        })
                        .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //TODO : delete task
                                deleteTask(task.getId());
                            }
                        })
                        .create();
                dialog.show();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tes = editText.getText().toString();
                tembak = etCustom.getText().toString();
                if (!tes.equals("")){
                    nomorhp = filterno(tes);
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.createObject(Task.class, UUID.randomUUID().toString())
                                    .setName(editText.getText().toString());
                        }
                    });
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/"+nomorhp+"?text="+tembak));
                    startActivity(browserIntent);
                }if ((tes.equals(""))){
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage("No telepon tidak boleh kosong")
                            .setPositiveButton("Ok", null)
                            .create();
                }
            }
        });

        etCustom.setVisibility(View.GONE);

        btnCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etCustom.setVisibility(View.VISIBLE);
                btnCustom.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.about){
            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
            MainActivity.this.startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private String filterno(String nohp) {
        String no = "";
        if (nohp.startsWith("+62")){
            no = nohp.replaceFirst("\\+62", "62");
        }
        if (nohp.startsWith("0")){
            no = nohp.replaceFirst("0", "62");
        }if (nohp.startsWith("62")){
            no = nohp;
        }
        return no;
    }

    private void deleteTask(final String taskId) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Task.class).equalTo("id", taskId)
                        .findFirst()
                        .deleteFromRealm();
            }
        });
    }

}
