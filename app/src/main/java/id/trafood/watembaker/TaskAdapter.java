package id.trafood.watembaker;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.TextView;


import io.realm.OrderedRealmCollection;
import io.realm.RealmBaseAdapter;

public class TaskAdapter extends RealmBaseAdapter<Task> implements ListAdapter {
    private MainActivity activity;

    private static class ViewHolder{
        TextView taskName;
        TextView taskDelete;
    }

    TaskAdapter(MainActivity activity, OrderedRealmCollection<Task>data){
        super(data);
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final ViewHolder viewHolder;
        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_nomerteleoin, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.taskName = (TextView) convertView.findViewById(R.id.tvNotelepon);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (adapterData != null){
            Task task = adapterData.get(position);
            viewHolder.taskName.setText(task.getName());

        }

        return convertView;
    }


}
